<?php

namespace App\Entity;

use Doctrine\ORM\Mapping AS ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="booking")
 **/
class Booking
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @var \App\Entity\User $user
     *
     * @ORM\ManyToOne(targetEntity="\App\Entity\User", inversedBy="bookings", cascade={"persist", "merge"})
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    protected $user;


    /**
     * @var \App\Entity\Office $office
     *
     * @ORM\ManyToOne(targetEntity="\App\Entity\Office", inversedBy="bookings", cascade={"persist", "merge"})
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="office_id", referencedColumnName="id")
     * })
     */
    protected $office;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime", name="startsAt")
     */
    protected $startsAt;

    /**
     * @ORM\Column(type="datetime", name="endsAt")
     */
    protected $endsAt;

    /**
     * @var string
     * @ORM\Column(type="integer", name="status", nullable=true)
     */
    protected $status;

    /**
     * @var integer
     * @ORM\Column(type="boolean", name="sick")
     */
    protected $sick;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->bookings = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set endsAt
     *
     * @param \DateTime $endsAt
     *
     * @return Booking
     */
    public function setEndsAt($endsAt)
    {
        $this->endsAt = $endsAt;

        return $this;
    }

    /**
     * Get endsAt
     *
     * @return \DateTime
     */
    public function getEndsAt()
    {
        return $this->endsAt;
    }


    /**
     * Set startsAt
     *
     * @param \DateTime $startsAt
     *
     * @return Booking
     */
    public function setStartsAt($startsAt)
    {
        $this->startsAt = $startsAt;

        return $this;
    }

    /**
     * Get startsAt
     *
     * @return \DateTime
     */
    public function getStartsAt()
    {
        return $this->startsAt;
    }

    /**
     * Set Status
     *
     * @param integer $status
     *
     * @return Booking
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set Sick
     *
     * @param integer $sick
     *
     * @return Booking
     */
    public function setSick($sick)
    {
        $this->sick = $sick;

        return $this;
    }

    /**
     * Get sick
     *
     * @return integer
     */
    public function getSick()
    {
        return $this->sick;
    }

    /**
     * Set user
     *
     * @param \App\Entity\User $user
     *
     * @return Booking
     */
    public function setUser(\App\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set office
     *
     * @param \App\Entity\Office $office
     *
     * @return Booking
     */
    public function setOffice(\App\Entity\Office $office = null)
    {
        $this->office = $office;

        return $this;
    }

    /**
     * Get office
     *
     * @return \App\Entity\Office
     */
    public function getOffice()
    {
        return $this->office;
    }

}