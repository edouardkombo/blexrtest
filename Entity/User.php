<?php

namespace App\Entity;

use Doctrine\ORM\Mapping AS ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 **/
class User
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", name="lastname", length=20)
     */
    protected $lastname;

    /**
     * @var string
     * @ORM\Column(type="string", name="firstname", length=20)
     */
    protected $firstname;

    /**
     * @var string
     * @ORM\Column(type="string", name="email", length=30)
     */
    protected $email;

    /**
     * @var string
     * @ORM\Column(type="string", name="password", length=60, nullable=true)
     */
    protected $password;

    /**
     * @var string
     * @ORM\Column(type="string", name="token", length=50, nullable=true)
     */
    protected $token;

    /**
     * @var string
     * @ORM\Column(type="string", name="role",length=25)
     */
    protected $role;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="integer", name="email_access_granted",nullable=true)
     */
    protected $emailAccessGranted;

    /**
     * @ORM\Column(type="integer", name="git_repository_granted",nullable=true)
     */
    protected $gitRepositoryGranted;

    /**
     * @ORM\Column(type="integer", name="trello_access_granted",nullable=true)
     */
    protected $trelloAccessGranted;

    /**
     * @ORM\Column(type="integer", name="microsoft_office_license",nullable=true)
     */
    protected $microsoftOfficeLicense;

    /**
     * @ORM\OneToMany(targetEntity="\App\Entity\Booking", mappedBy="user", cascade={"persist", "remove", "merge"})
     */
    protected $bookings;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->bookings = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lastname
     *
     * @param string lastname
     *
     * @return User
     */
    public function setLastName($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastname;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return User
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return User
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set emailAccessGranted
     *
     * @param integer $emailAccessGranted
     *
     * @return User
     */
    public function setEmailAccessGranted($emailAccessGranted)
    {
        $this->emailAccessGranted = $emailAccessGranted;

        return $this;
    }

    /**
     * Get emailAccessGranted
     *
     * @return integer
     */
    public function getEmailAccessGranted()
    {
        return $this->emailAccessGranted;
    }

    /**
     * Set gitRepositoryGranted
     *
     * @param integer $gitRepositoryGranted
     *
     * @return User
     */
    public function setGitRepositoryGranted($gitRepositoryGranted)
    {
        $this->gitRepositoryGranted = $gitRepositoryGranted;

        return $this;
    }

    /**
     * Get gitRepositoryGranted
     *
     * @return integer
     */
    public function getGitRepositoryGranted()
    {
        return $this->gitRepositoryGranted;
    }

    /**
     * Set trelloAccessGranted
     *
     * @param integer $trelloAccessGranted
     *
     * @return User
     */
    public function setTrelloAccessGranted($trelloAccessGranted)
    {
        $this->trelloAccessGranted = $trelloAccessGranted;

        return $this;
    }

    /**
     * Get trelloAccessGranted
     *
     * @return integer
     */
    public function getTrelloAccessGranted()
    {
        return $this->trelloAccessGranted;
    }

    /**
     * Set microsoftOfficeLicense
     *
     * @param integer $microsoftOfficeLicense
     *
     * @return User
     */
    public function setMicrosoftOfficeLicense($microsoftOfficeLicense)
    {
        $this->microsoftOfficeLicense = $microsoftOfficeLicense;

        return $this;
    }

    /**
     * Get microsoftOfficeLicense
     *
     * @return integer
     */
    public function getMicrosoftOfficeLicense()
    {
        return $this->microsoftOfficeLicense;
    }

    /**
     * Add booking
     *
     * @param \App\Entity\Booking $booking
     *
     * @return User
     */
    public function addBooking(\App\Entity\Booking $booking)
    {
        $this->bookings[] = $booking;

        return $this;
    }

    /**
     * Remove booking
     *
     * @param \App\Entity\Booking $booking
     */
    public function removeBooking(\App\Entity\Booking $booking)
    {
        $this->bookings->removeElement($booking);
    }

    /**
     * Get bookings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBookings()
    {
        return $this->bookings;
    }

}