<?php

/**
 * FIRST! Create admin from CLI and then delete this file
 *
 * Format: php cli-create-admin lastname firstname email password
 *
 * example: php cli-create-admin Kombo edouard edouard.kombo@gmail.com yourpassword
 */

define('__ROOT_PATH__', realpath(dirname(__FILE__) . '/' ));
require_once "vendor/autoload.php"; //Autoload composer dependencies
require_once "config/ProjectAutoloaderConfig.php"; //Autoload project classes (Simplier than yours before)
require_once "config/Environment.php"; //Also delete cache directory when in dev mode
require_once "config/DoctrineConfig.php"; //Instantiate Doctrine object

use Hautelook\Phpass\PasswordHash; //password hasher algorythm

/**
 * Configure the password hasher object
 */
$passwordHasher = new PasswordHash($environmentConfig['site']['password_iteration'],false);


if ((php_sapi_name() == "cli") && is_array($argv) && count($argv) > 4) {

    $lastname = $argv[1];
    $firstname = $argv[2];
    $email = $argv[3];
    $_password = $argv[4];

    $password       = $passwordHasher->HashPassword($_password);
    $passwordMatch  = $passwordHasher->CheckPassword($_password, $password);

    if (true === $passwordMatch) {
        $user = new \App\Entity\User();
        $user->setEmail($email);
        $user->setLastName($lastname);
        $user->setFirstName($firstname);
        $user->setPassword($password);
        $user->setRole('ROLE_ADMIN');
        $user->setCreatedAt(new DateTime());
        $user->setEmailAccessGranted(true);

        $entityManager->persist($user);
        $entityManager->flush();

        echo "Admin user successfully Created, you can now log in !"."\r\n";
    } else {

        echo "Problem issued in password hashing";
    }

} else {
    echo "Admin not created, please specify cli arguments !"."\r\n";
}