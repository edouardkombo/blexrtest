<?php

/**
 * @Author: Edouardd Kombo
 */

namespace App\Controller;


use \App\Lib\ServiceContainer;
use Symfony\Component\HttpFoundation\RedirectResponse;


/**
 * Class ErrorController
 * @package App\Controller
 */
class ErrorController {

    protected $dependencyInjector;

    public function __construct(ServiceContainer $serviceContainer)
    {
        $this->dependencyInjector = $serviceContainer;
    }

    /**
     * Show 401 error page
     *
     * notAuthorized Action
     */
    public function notAuthorizedAction()
    {
        $twig = $this->dependencyInjector->get('template.engine');
        return $twig->render('/error/401.html.twig');
    }
}