<?php

/**
 * @author: Edouard Kombo
 */

namespace App\Controller;

use \Doctrine\ORM\EntityManager;
use \App\Entity\User as User;
use \App\Lib\ServiceContainer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use JasonGrimes\Paginator;

/**
 * Class UserController
 * @package App\Controller
 */
class UserController {

    protected $dependencyInjector;

    public function __construct(ServiceContainer $serviceContainer)
    {
        $this->dependencyInjector = $serviceContainer;
    }

    /**
     * Show administration page
     *
     * index Action
     */
    public function indexAction()
    {
        $twig = $this->dependencyInjector->get('template.engine');
        return $twig->render('/user/index.html.twig');
    }

    /**
     * Get all users
     *
     * Pagination is fixed to 10 users
     *
     * @param integer $id
     * @return mixed
     */
    public function listAction($id = null)
    {
        $em     = $this->dependencyInjector->get('entity.manager');

        //Count the total of users in database
        $totalUsers = $em->createQueryBuilder('u')
            ->select('count(u.id) as total')
            ->from('\App\Entity\User', 'u')
            ->getQuery()
            ->getSingleResult();

        //Pagination tools
        $totalItems = $totalUsers['total'];
        $itemsPerPage = 10;
        $currentPage = $id;
        $urlPattern = '/user/list/(:num)';

        $users = $em->createQueryBuilder('u')
            ->select('u.id, u.lastname, u.firstname, u.email, u.createdAt, u.role, u.emailAccessGranted')
            ->from('\App\Entity\User','u')
            ->setFirstResult( $currentPage - 1 )
            ->setMaxResults( $itemsPerPage )
            ->getQuery()
            ->getResult();

        $paginator = new Paginator($totalItems, $itemsPerPage, $currentPage, $urlPattern);

        //Generate a csrf token and insert inside the form in an hidden field
        $csrfManager    = $this->dependencyInjector->get('csrf.manager');

        $twig = $this->dependencyInjector->get('template.engine');
        return $twig->render('/user/list.html.twig',array(
            'total' => $totalItems,
            'users' => $users,
            'paginator' => $paginator,
            'csrf_token' => $csrfManager->getToken()
        ));
    }

    /**
     * Show form to edit a user
     *
     * The logged in user must have the same id, or it must be an admin
     * If not, we trigger a 403 http status
     *
     * edit Action
     */
    public function editAction($id)
    {
        $passport       = $this->dependencyInjector->get('passport.manager');
        $currentUser    = $passport->getUser();

        //If user is the owner or if it is Admin, we can edit
        if (($id == $currentUser->getId()) || ($passport->hasRole('ROLE_ADMIN'))) {

            $em     = $this->dependencyInjector->get('entity.manager');
            $user   = $em->getRepository('\App\Entity\User')->find($id);

            if ($user instanceof \App\Entity\User) {

                //Generate a csrf token and insert inside the form in an hidden field
                $csrfManager    = $this->dependencyInjector->get('csrf.manager');

                $twig = $this->dependencyInjector->get('template.engine');
                return $twig->render('/user/edit.html.twig', array(
                    'user' => $user,
                    'csrf_token' => $csrfManager->getToken()
                ));
            }

        } else {
            return new RedirectResponse(403);
        }
    }

    /**
     * Update a user after edition
     *
     * We ensure that the logged in user has the smae id than sent by POST method, or is admin
     *
     * update Action
     */
    public function updateAction()
    {
        //Call necessary dependencies
        $request        = Request::createFromGlobals();

        //Retrieve post values
        $id             = $request->request->get('id');
        $lastname       = $request->request->get('lastname');
        $firstname      = $request->request->get('firstname');
        $email          = $request->request->get('email');
        $_password      = $request->request->get('password');

        //By default if role is empty, we set ROLE_USER
        $role           = (empty($request->request->get('role'))) ? 'ROLE_USER' : $request->request->get('role');

        $emailAccessGranted           = ($request->request->get('email_access_granted') === 'on') ? true : false;
        $gitRepositoryGranted         = ($request->request->get('git_repository_granted') === 'on') ? true : false;
        $microsoftOfficeLicense       = ($request->request->get('microsoft_office_license') === 'on') ? true : false;
        $trelloAccessGranted          = ($request->request->get('trello_access_granted') === 'on') ? true : false;

        //Retrieve specified user
        $em             = $this->dependencyInjector->get('entity.manager');
        $user           = $em->getRepository('\App\Entity\User')->find($id);

        //if password is empty
        if (empty($_password)) {
            //let as it
            $password       = $_password;
            $passwordMatch  = true;
        } else {
            //Encode and check
            $passwordHasher = $this->dependencyInjector->get('password.manager');
            $password       = $passwordHasher->HashPassword($_password);
            $passwordMatch  = $passwordHasher->CheckPassword($_password, $password);
        }

        //Retrieve the current logged in user
        $passport       = $this->dependencyInjector->get('passport.manager');
        $currentUser    = $passport->getUser();

        //If passwords is ok, and the current user has same id, or user is admin, we can update
        if ((true === $passwordMatch) && ($id == $currentUser->getId()) || ($passport->hasRole('ROLE_ADMIN'))) {

            //Add new entries
            $user->setLastName($lastname);
            $user->setFirstName($firstname);
            $user->setEmail($email);
            $user->setRole($role);

            //Do not change password if empty
            if (!empty($password)) {
                $user->setPassword($password);
            }


            //Add datas related to admin only
            if ($passport->hasRole('ROLE_ADMIN')) {
                $user->setEmailAccessgranted($emailAccessGranted);
                $user->setGitRepositorygranted($gitRepositoryGranted);
                $user->setMicrosoftOfficeLicense($microsoftOfficeLicense);
                $user->setTrelloAccessGranted($trelloAccessGranted);
            }

            //Save the user
            $em->persist($user);

            if (is_null($em->flush())) {
                $datas = array('message' => 'User successfully updated !', 'status' => 'success');
            } else {
                $datas = array('message' => 'Something went wrong, please try again later !', 'status' => 'error');
            }

            $response = new JsonResponse();
            $response->setData($datas);
            return $response->send();

        } else {
            return new RedirectResponse('/user', 403);
        }
    }

    /**
     * Show the user creation form
     *
     * create Action
     */
    public function createAction()
    {
        //Generate a csrf token and insert inside the form in an hidden field
        $csrfManager    = $this->dependencyInjector->get('csrf.manager');

        $twig = $this->dependencyInjector->get('template.engine');
        return $twig->render('/user/create.html.twig', array(
            'csrf_token' => $csrfManager->getToken()
        ));
    }

    /**
     * Create a user after form sent
     *
     * We ensure that the logged in user has the smae id than sent by POST method, or is admin
     * We don't need to create a password as it was not requested in the test
     *
     * update Action
     */
    public function addAction()
    {
        //Call necessary dependencies
        $request        = Request::createFromGlobals();

        //Retrieve entity manager
        $em             = $this->dependencyInjector->get('entity.manager');

        //Retrieve post values
        $id             = $request->request->get('id');
        $lastname       = $request->request->get('lastname');
        $firstname      = $request->request->get('firstname');
        $email          = $request->request->get('email');

        //if no roles sent, we assign ROLE_USER by default
        $role           = (empty($request->request->get('role'))) ? 'ROLE_USER' : $request->request->get('role');

        $user = new User();

        //Add new entries
        $user->setLastName($lastname);
        $user->setFirstName($firstname);
        $user->setEmail($email);
        $user->setRole($role);
        $user->setCreatedAt(new \DateTime());

        //Save the user
        $em->persist($user);

        if (is_null($em->flush())) {
            $datas = array('message' => 'User successfully created !', 'status' => 'success');
        } else {
            $datas = array('message' => 'Something went wrong, please try again later !', 'status' => 'error');
        }

        $response = new JsonResponse();
        $response->setData($datas);
        return $response->send();
    }

    /**
     * Send email to user in order to let him register
     *
     * We ensure that the logged in user has the smae id than sent by POST method, or is admin
     * We don't need to create a password as it was not requested in the test
     *
     * send mail Action
     */
    public function sendMailAction()
    {
        //Call necessary dependencies
        $request        = Request::createFromGlobals();


        //Retrieve dependency instances
        $mail           = $this->dependencyInjector->get('mail.manager');
        $twig           = $this->dependencyInjector->get('template.engine');
        $em             = $this->dependencyInjector->get('entity.manager');
        $passwordHasher = $this->dependencyInjector->get('password.manager');

        //Retrieve post values
        $id             = $request->request->get('id');

        $user = $em->getRepository('\App\Entity\User')->find($id);

        //We first check if user exist
        if ($user instanceof \App\Entity\User) {
            //Generate password
            $newPassword = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') , 0 , 10 );

            //Generate user token
            $token = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') , 0 , 10 );

            //Encode password and check it
            $password       = $passwordHasher->HashPassword($newPassword);
            $passwordMatch  = $passwordHasher->CheckPassword($newPassword, $password);


            //Update user and send email
            $user->setPassword($password);
            $user->setToken($token);

            $em->persist($user);

            if (is_null($em->flush())) {

                //Send email to user
                $mail->addAddress($user->getEmail(), $user->getLastName() . ' ' . $user->getFirstName()); // Add a recipient
                $mail->Subject = 'Activate your Blexr account';
                $mail->Body    = $twig->render('/mail/invite_user.html.twig', array(
                    'email' => $user->getEmail(),
                    'password' => $newPassword,
                    'url'   => $request->getSchemeAndHttpHost() . "?user_token=$token"
                ));

                if(!$mail->send()) {
                    $datas = array('message' => "Message not sent ! $mail->ErrorInfo", 'status' => 'error');
                } else {
                    $datas = array('message' => 'Message sent !', 'status' => 'success');
                }
            } else {
                $datas = array('message' => 'Message not sent due to user !', 'status' => 'error');
            }
        } else {
            $datas = array('message' => 'Message not sent, user not foundr !', 'status' => 'error');
        }


        $response = new JsonResponse();
        $response->setData($datas);
        return $response->send();
    }

    /**
     * log out the current user
     * Get the passport service, check if the user is authenticated, log out the user and redirect to home page
     * If the user isn't authenticated, return also to home page
     */
    public function logOutAction()
    {
        $passport       = $this->dependencyInjector->get('passport.manager');

        if (true === $passport->isAuthenticated()) {

            //if something doesn't work during logout process throw and catch exception
            try {
                if (true === $passport->logOut()) {
                    return new RedirectResponse('/');
                } else {
                    throw new \Exception('Something wrong happened during logout !');
                }

            } catch (\Exception $e) {
                return $e->getMessage();
            }

        } else {
            return new RedirectResponse('/');
        }
    }
}