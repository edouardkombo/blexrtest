<?php

/**
 * @author: Edouard Kombo
 */

namespace App\Controller;

use \Doctrine\ORM\EntityManager;
use \App\Entity\Office as Office;
use \App\Lib\ServiceContainer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use JasonGrimes\Paginator;

/**
 * Class OfficeController
 * @package App\Controller
 */
class OfficeController {

    protected $dependencyInjector;

    public function __construct(ServiceContainer $serviceContainer)
    {
        $this->dependencyInjector = $serviceContainer;
    }

    /**
     * Get all users
     *
     * Pagination is fixed to 10 users
     *
     * @param integer $id
     * @return mixed
     */
    public function listAction($id = null)
    {
        $em     = $this->dependencyInjector->get('entity.manager');

        //Count the total of users in database
        $totalOffices = $em->createQueryBuilder('o')
            ->select('count(o.id) as total')
            ->from('\App\Entity\Office', 'o')
            ->getQuery()
            ->getSingleResult();

        //Pagination tools
        $totalItems = $totalOffices['total'];
        $itemsPerPage = 10;
        $currentPage = $id;
        $urlPattern = '/office/list/(:num)';

        $offices = $em->createQueryBuilder('o')
            ->select('o.id, o.town')
            ->from('\App\Entity\Office','o')
            ->setFirstResult( $currentPage - 1 )
            ->setMaxResults( $itemsPerPage )
            ->getQuery()
            ->getResult();

        $paginator = new Paginator($totalItems, $itemsPerPage, $currentPage, $urlPattern);

        $twig = $this->dependencyInjector->get('template.engine');
        return $twig->render('/office/list.html.twig',array(
            'total' => $totalItems,
            'offices' => $offices,
            'paginator' => $paginator
        ));
    }

    /**
     * Show form to edit an office
     *
     * The logged in user must have the same id, or it must be an admin
     * If not, we trigger a 403 http status
     *
     * edit Action
     */
    public function editAction($id)
    {
        $em     = $this->dependencyInjector->get('entity.manager');
        $office   = $em->getRepository('\App\Entity\Office')->find($id);

        if ($office instanceof \App\Entity\Office) {

            //Generate a csrf token and insert inside the form in an hidden field
            $csrfManager    = $this->dependencyInjector->get('csrf.manager');

            $twig = $this->dependencyInjector->get('template.engine');
            return $twig->render('/office/edit.html.twig', array(
                'office' => $office,
                'csrf_token' => $csrfManager->getToken()
            ));
        }
    }

    /**
     * Update an office after edition
     *
     *
     * update Action
     */
    public function updateAction()
    {
        //Call necessary dependencies
        $request        = Request::createFromGlobals();

        //Retrieve post values
        $id             = $request->request->get('id');
        $town           = $request->request->get('town');

        //Retrieve specified user
        $em             = $this->dependencyInjector->get('entity.manager');
        $office           = $em->getRepository('\App\Entity\Office')->find($id);


        //Update town name
        $office->setTown($town);

        //Save
        $em->persist($office);

        if (is_null($em->flush())) {
            $datas = array('message' => 'Office successfully updated !', 'status' => 'success');
        } else {
            $datas = array('message' => 'Something went wrong, please try again later !', 'status' => 'error');
        }

        $response = new JsonResponse();
        $response->setData($datas);
        return $response->send();
    }

    /**
     * Show the office creation form
     *
     * create Action
     */
    public function createAction()
    {
        //Generate a csrf token and insert inside the form in an hidden field
        $csrfManager    = $this->dependencyInjector->get('csrf.manager');

        $twig = $this->dependencyInjector->get('template.engine');
        return $twig->render('/office/create.html.twig', array(
            'csrf_token' => $csrfManager->getToken()
        ));
    }

    /**
     * Create an office after form sent
     *
     * update Action
     */
    public function addAction()
    {
        //Call necessary dependencies
        $request        = Request::createFromGlobals();

        //Retrieve entity manager
        $em             = $this->dependencyInjector->get('entity.manager');

        //Retrieve post values
        $id             = $request->request->get('id');
        $town           = $request->request->get('town');

        $office = new Office();

        //Add new entries
        $office->setTown($town);

        //Save
        $em->persist($office);

        if (is_null($em->flush())) {
            $datas = array('message' => 'Office successfully created !', 'status' => 'success');
        } else {
            $datas = array('message' => 'Something went wrong, please try again later !', 'status' => 'error');
        }

        $response = new JsonResponse();
        $response->setData($datas);
        return $response->send();
    }
}