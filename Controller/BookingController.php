<?php

/**
 * @author: Edouard Kombo
 */

namespace App\Controller;

use \Doctrine\ORM\EntityManager;
use \App\Entity\Booking as Booking;
use \App\Lib\ServiceContainer;
use \App\Lib\Helper as Helper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use JasonGrimes\Paginator;

/**
 * Class BookingController
 * @package App\Controller
 */
class BookingController {

    protected $dependencyInjector;

    public function __construct(ServiceContainer $serviceContainer)
    {
        $this->dependencyInjector = $serviceContainer;
    }

    /**
     * Get current user bookings
     *
     * Pagination is fixed to 10 bookings
     *
     * @return mixed
     */
    public function showAction()
    {
        //Call necessary dependencies
        $passport       = $this->dependencyInjector->get('passport.manager');
        $em             = $this->dependencyInjector->get('entity.manager');
        $csrfManager    = $this->dependencyInjector->get('csrf.manager');

        $user           = $passport->getUser();

        //Retrieve user bookings
        $bookings       = $em->getRepository('\App\Entity\Booking')->findBy(array('user' => $user->getId()));

        $twig = $this->dependencyInjector->get('template.engine');
        return $twig->render('/booking/index.html.twig',array(
            'total' => count($bookings),
            'bookings' => $bookings,
            'csrf_token' => $csrfManager->getToken()
        ));
    }

    /**
     * Get all bookings
     *
     * Pagination is fixed to 10 bookings
     *
     * @param integer $id
     * @return mixed
     */
    public function listAction($id = null)
    {
        //Call necessary dependencies
        $request        = Request::createFromGlobals();

        $filter = $request->query->get('filter');

        $csrfManager    = $this->dependencyInjector->get('csrf.manager');
        $em     = $this->dependencyInjector->get('entity.manager');

        //Count the total of bookings in database
        $totalBookings = $em->createQueryBuilder('b')
            ->select('count(b.id) as total')
            ->from('\App\Entity\Booking', 'b')
            ->getQuery()
            ->getSingleResult();

        //Pagination tools
        $totalItems = $totalBookings['total'];
        $itemsPerPage = 10;
        $currentPage = $id;
        $urlPattern = "/booking/list/(:num)?filter=$filter";

        //We filter our query depending on select option
        //We will use partial inside select query to retrieve the full collection, instead of a single array
        if (isset($filter) && ($filter !== 'all')) {
            $bookings = $em->createQueryBuilder('o')
                ->select('partial b.{id, createdAt, startsAt, endsAt, status}')
                ->from('\App\Entity\Booking','b')
                ->where('b.status = :filter')
                ->setParameter('filter', $filter)
                ->setFirstResult( $currentPage - 1 )
                ->setMaxResults( $itemsPerPage )
                ->getQuery()
                ->getResult();
        } else {
            $bookings = $em->createQueryBuilder('o')
                ->select('partial b.{id, createdAt, startsAt, endsAt, status}')
                ->from('\App\Entity\Booking','b')
                ->setFirstResult( $currentPage - 1 )
                ->setMaxResults( $itemsPerPage )
                ->getQuery()
                ->getResult();
        }

        $paginator = new Paginator($totalItems, $itemsPerPage, $currentPage, $urlPattern);

        $twig = $this->dependencyInjector->get('template.engine');
        return $twig->render('/booking/list.html.twig',array(
            'total' => $totalItems,
            'bookings' => $bookings,
            'filter' => $filter,
            'paginator' => $paginator,
            'csrf_token' => $csrfManager->getToken()
        ));
    }


    /**
     * Update booking (moderate)
     *
     * @return mixed
     */
    public function updateAction()
    {
        //Call necessary dependencies
        $passport       = $this->dependencyInjector->get('passport.manager');
        $em             = $this->dependencyInjector->get('entity.manager');
        $request        = Request::createFromGlobals();

        $id             = $request->request->get('id');
        $status         = $request->request->get('status');

        //Retrieve specified booking
        $booking        = $em->getRepository('\App\Entity\Booking')->find($id);

        if ($booking instanceof \App\Entity\Booking) {

            //Retrieve current user
            $currentUser = $passport->getUser();

            //Get booking related user
            $user   = $booking->getUser();

            //If booking owner is the same than current owner or is admin we can allow
            if (($user->getId() == $currentUser->getId()) || ($passport->hasRole('ROLE_ADMIN'))) {

                //We only allow admin to approve or reject requests
                //We only allow owner to cancel its request
                if (($passport->hasRole('ROLE_ADMIN') && ($status == '1' || $status == '2'))
                || ($passport->hasRole('ROLE_USER') && ($user->getId() == $currentUser->getId()) && ($status == '3'))) {

                    $booking->setStatus($status);

                    //Save
                    $em->persist($booking);

                    if (is_null($em->flush())) {

                        //Alert user about moderation if status is only 1 or 2
                        if ($status == '1' || $status == '2') {
                            $datas = $this->sendMailAction($booking);

                        } else {

                            //show message and redirect user
                            $datas = array(
                                'message' => 'Work at home request successfully cancelled',
                                'status' => 'success',
                                'redirect' => '/booking'
                            );
                        }

                    } else {
                        $datas = array('message' => 'Something went wrong, please try again later !', 'status' => 'error');
                    }
                }

            } else {

                $datas = array('message' => 'Something went wrong, please try again later !',
                    'status' => 'error',
                    'redirect' => 403
                );
            }

        } else {
            $datas = array('message' => 'Something went wrong, please try again later !',
                'status' => 'error',
                'redirect' => 403
            );
        }

        $response = new JsonResponse();
        $response->setData($datas);
        return $response->send();
    }

    /**
     * Show the booking creation form
     *
     * create Action
     */
    public function createAction()
    {
        //Generate a csrf token and insert inside the form in an hidden field
        $csrfManager    = $this->dependencyInjector->get('csrf.manager');

        $em             = $this->dependencyInjector->get('entity.manager');

        //Count the total of offices in database
        $totalOffices = $em->createQueryBuilder('o')
            ->select('count(o.id) as total')
            ->from('\App\Entity\Office', 'o')
            ->getQuery()
            ->getSingleResult();

        //Retrieve all offices
        $offices        = $em->getRepository('\App\Entity\Office')->findAll();

        $twig = $this->dependencyInjector->get('template.engine');
        return $twig->render('/booking/create.html.twig', array(
            'total' => $totalOffices['total'],
            'offices' => $offices,
            'csrf_token' => $csrfManager->getToken()
        ));
    }

    /**
     * Create a booking after form sent
     *
     * update Action
     */
    public function addAction()
    {
        //Call necessary dependencies
        $request        = Request::createFromGlobals();

        //Retrieve dependencies
        $em             = $this->dependencyInjector->get('entity.manager');
        $passport       = $this->dependencyInjector->get('passport.manager');
        $helper         = new Helper();

        //Retrieve post values
        $from           = $request->request->get('from');
        $to             = $request->request->get('to');
        $sick           = ($request->request->get('sick') === 'on') ? true : false;
        $officeId         = $request->request->get('office');

        $booking        = new Booking();
        $_user          = $passport->getUser();
        $user           = $em->getRepository('\App\Entity\User')->find($_user->getId());
        $office         = $em->getRepository('\App\Entity\Office')->find($officeId);

        //Convert given dates to datetime
        $fromDate = $helper->createDateFromFormat('m/d/Y H:i A', $from);
        $toDate = $helper->createDateFromFormat('m/d/Y H:i A', $to);

        //Call the booking logic
        $bookingLogicResult = $helper->bookingLogic($fromDate, $toDate, $sick);

        //If logic returns array, we have errors and then we show them
        if (is_array($bookingLogicResult)) {

            $response = new JsonResponse();
            $response->setData($bookingLogicResult);
            return $response->send();

        } else {
            //Add new entries
            $booking->setCreatedAt(new \DateTime());
            $booking->setStartsAt($fromDate);
            $booking->setEndsAt($toDate);
            $booking->setStatus(0);
            $booking->setSick($sick);

            $booking->setUser($user);
            $booking->setOffice($office);

            $em->persist($booking);

            //Save
            if (is_null($em->flush())) {
                $datas = array('message' => 'Booking successfully created !', 'status' => 'success');
            } else {
                $datas = array('message' => 'Something went wrong, please try again later !', 'status' => 'error');
            }

            $response = new JsonResponse();
            $response->setData($datas);
            return $response->send();
        }
    }

    /**
     * Alert user about its work at home request moderation
     *
     * We ensure that the logged in user has the smae id than sent by POST method, or is admin
     * We don't need to create a password as it was not requested in the test
     *
     * send mail Action
     */

    /**
     * Alert user about its work at home request moderation
     *
     * @param $booking \App\Entity\Booking
     * @return mixed
     */
    public function sendMailAction($booking)
    {
        //Call necessary dependencies
        $request        = Request::createFromGlobals();

        //Retrieve dependency instances
        $mail           = $this->dependencyInjector->get('mail.manager');
        $twig           = $this->dependencyInjector->get('template.engine');

        //Retrieve related user and office
        $user           = $booking->getUser();
        $office         = $booking->getOffice();

        //Send email to user
        $mail->addAddress($user->getEmail(), $user->getLastName() . ' ' . $user->getFirstName()); // Add a recipient
        $mail->Subject = 'Your work at home request has been moderated';
        $mail->Body    = $twig->render('/mail/work_at_home_moderation.html.twig', array(
            'firstname' => $user->getFirstName(),
            'from' => $booking->getStartsAt(),
            'to' => $booking->getEndsAt(),
            'town' => $office->getTown(),
            'status' => $booking->getStatus(),
            'url'   => $request->getSchemeAndHttpHost()
        ));

        if(!$mail->send()) {
            $datas = array('message' => "Message not sent ! $mail->ErrorInfo", 'status' => 'error');
        } else {
            $datas = array('message' => 'Message sent !', 'status' => 'success', 'redirect' => '/booking/list/1');
        }

        return $datas;
    }
}