<?php

/**
 * @Author: Edouard Kombo
 */

namespace App\Controller;

use \Doctrine\ORM\EntityManager;
use \App\Entity\User as User;
use \App\Lib\ServiceContainer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class LoginController
 * @package App\Controller
 */
class LoginController {

    protected $dependencyInjector;

    public function __construct(ServiceContainer $serviceContainer)
    {
        $this->dependencyInjector = $serviceContainer;
    }

    /**
     * Get login credendtials and check if user exists in database
     *
     * index Action
     */
    public function indexAction()
    {

        //Call necessary dependencies
        $request        = Request::createFromGlobals();
        $em             = $this->dependencyInjector->get('entity.manager');
        $passwordHasher = $this->dependencyInjector->get('password.manager');

        //Retrieve post values
        $email          = $request->request->get('email');
        $_password      = $request->request->get('password');
        $urlToken       = $request->request->get('user_token');

        //Retrieve user by email
        $user           = $em->getRepository('\App\Entity\User')->findOneBy(array('email' => $email));

        //If user has been found
        if (is_object($user)) {

            //Check that provided password matches password in the database
            $passwordMatch  = $passwordHasher->CheckPassword($_password, $user->getPassword());

            //If Password matches
            if (true === $passwordMatch) {

                //If user token in database is empty, user has granted email access, we allow him to authentify
                if (empty($user->getToken())) {

                    //Authenticate first the user
                    $passport       = $this->dependencyInjector->get('passport.manager');
                    $passport->authenticate($user);

                    //Return specified message to view
                    $datas = array(
                        'message' => 'User successfully logged in !',
                        'status' => 'success',
                        'redirect' => '/'
                    );

                } else {
                    //Token in database is not empty

                    //If database token and url token match, User logs for the first time, we grant him
                    if ($user->getToken() === $urlToken) {

                        //We empty the token and set email access granted to true, for this user
                        $user->setToken('');
                        $user->setEmailAccessGranted(true);
                        $em->persist($user);
                        $em->flush();

                        //Authenticate the user
                        $passport       = $this->dependencyInjector->get('passport.manager');
                        $passport->authenticate($user);

                        //Return specified message to view
                        $datas = array(
                            'message' => 'User successfully logged in !',
                            'status' => 'success',
                            'redirect' => '/'
                        );

                    } else {

                        //This is a bad token, we can't grant access to this user, maybe steal attempt
                        //Return specified message to view
                        $datas = array(
                            'message' => 'Bad token, please be sure to click on the link given in the email',
                            'status' => 'error'
                        );
                    }
                }

            } else {
                //If Password doesn't match, prepare to send an error
                $datas = array('message' => 'Password is incorrect !', 'status' => 'error');
            }

        } else {
            //If user is empty, then email or password is incorrect
            $datas = array('message' => 'Wrong credentials sent !', 'status' => 'error');
        }

        $response = new JsonResponse();
        $response->setData($datas);
        return $response->send();
    }

    /**
     * Show a specific user
     *
     * @param integer $id
     */
    public function showAction($id)
    {
        echo "Hello World !";
    }
}