<?php

/**
 * It is a very good practice to deal with namespaces inside a project
 * So, I decided to add a namespace at the beginning of the project
 */
namespace App;

/**
 * this is the index file which should be used as the initiator for all calls in the system
 * @author Christopher Bartolo <chris@chrisbartolo.com>
 *
 * @Co-Author: Special message, maybe the simple framework was a "TRAP", but your basic MVC structure WAS AWFUL
 * Please excuse me if I tried to rebuild it with best coding standards, but I couldn't code with your basic structure
 **/

// start the session
session_start();

define('__ROOT_PATH__', realpath(dirname(__FILE__) . '/' ));

/**
 * @Author: Edouard Kombo
 *
 * NOTICE: Everything below has been developed or implemented by me
 */

    require_once "vendor/autoload.php"; //Autoload composer dependencies
    require_once "config/ProjectAutoloaderConfig.php"; //Autoload project classes (Simplier than yours before)

    /**
     * Here, I have included a specific php debugger (Tracy Nette) to show php errors in a beautiful way
     * it is easy for debugging
     */
    require_once "config/Environment.php"; //Also delete cache directory when in dev mode

    use App\Lib\ServiceContainer; //Simple dependency injection container I've written (very basic)
    use Hautelook\Phpass\PasswordHash; //password hasher algorythm


    /**
     * Inherit from PHPRouter to add our customs modification (instantiate each controller with dependency injection container)
     */
    use App\Lib\RouteCollection;
    use PHPRouter\Config;
    use App\Lib\Router;
    use App\Lib\Route;

    use Symfony\Component\HttpFoundation\Request;

    use App\Lib\Passport; //Object that will handle authentication and user informations

    use App\Lib\Acl; //Custom and basic Acl component

    require_once "config/DoctrineConfig.php"; //Instantiate Doctrine object

    /**
     * Configure the password hasher object
     */
    $passwordHasher = new PasswordHash($environmentConfig['site']['password_iteration'],false);

    /**
     * Instantiate passport object
     */
    $passport = new Passport();


    /**
     * Mailer object
     */
    $mail = new \PHPMailer;
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = $environmentConfig['mail']['host'];  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = $environmentConfig['mail']['username'];                 // SMTP username
    $mail->Password = $environmentConfig['mail']['password'];                           // SMTP password
    $mail->SMTPSecure = $environmentConfig['mail']['security'];                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = $environmentConfig['mail']['port'];                                    // TCP port to connect to
    $mail->addReplyTo($environmentConfig['mail']['replyTo'], 'Information');
    $mail->setFrom($environmentConfig['mail']['from'], $environmentConfig['mail']['sender_name']);
    $mail->isHTML(true);

    /**
     * Instantiate Twig template engine object
     * Because it is not a good id to insert php variables into view, it is a MVC pattern breaking rules,
     * we'll proceed like this
     *
     * We transmit session variable to twig to be able to call easier certain values
     */
    $loader = new \Twig_Loader_Filesystem(__DIR__ . '/view');
    $twig = new \Twig_Environment($loader, array(
        'cache' => __DIR__ . '/cache',
    ));
    //Create global Session value for twig, to call easily User datas later, and also Role permissions
    $twig->addGlobal('session', $passport->datas);

    /**
     * Instantiate The csrf manager object
     * Apply for a double submit token validation (see more here: https://www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF)_Prevention_Cheat_Sheet#Double_Submit_Cookies)
     */
    $csrfManager = new \Riimu\Kit\CSRF\CSRFHandler();
    $csrfManager->validateRequest(); //This method already generates a token

    /**
     * Inject twig instance into dependency injection container
     * Inject "Entity Manager" instance into dependency injection container from Doctrine (see config/DoctrineConfig.php)
     * Inject password manager also
     * Inject Passport object
     * Inject Csrf security manager instance
     * Inject Mail manager instance
     */
    $serviceContainer = new ServiceContainer();
    $serviceContainer->register('template.engine', $twig);
    $serviceContainer->register('entity.manager', $entityManager);
    $serviceContainer->register('password.manager', $passwordHasher);
    $serviceContainer->register('passport.manager', $passport);
    $serviceContainer->register('csrf.manager', $csrfManager);
    $serviceContainer->register('mail.manager', $mail);


    /**
     * Simply check if a user has permission to access some urls
     */
    $request        = Request::createFromGlobals();
    $siteUrl        = $request->getSchemeAndHttpHost();

    $acl = new Acl(__ROOT_PATH__ . '/config/acl.yml');
    $acl->setPassport($passport);
    $isSecured = $acl->secure();
    if (false === $isSecured) {
        //Due to http header issues, we redirect like that (Sorry)
        echo '<meta http-equiv="refresh" content="0;'.$siteUrl.'/401">';
    }

    /**
     * Load routes from config file in yaml
     * Pass the dependency container object to controller class construction
     * Match current Request and call specified controller and action
     *
     * HERE, we use our inheritance from PHPRouter
     */
    $config = Config::loadFromFile('config/routing.yml');
    $router = \App\Lib\Router::parseConfig($config);
    $router->serviceContainer = $serviceContainer;
    //We don't need this on cli mode as it will trigger php notices due to $_SERVER or $_REQUEST
    if (php_sapi_name() != "cli") {
        $router->matchCurrentRequest();
    }

