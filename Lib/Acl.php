<?php
/**
 * Created by Edouard Kombo.
 * @Author: Edouard Kombo
 * Date: 20/04/2016
 * Time: 10:42
 */

namespace App\Lib;

use \Symfony\Component\Yaml\Yaml as Yaml;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @author: Edouard Kombo
 * Class ACL
 * MIntercept request and manage Access control list
 * @package App\Lib
 */
class Acl
{
    /**
     * @var array $config
     */
    public $config;

    /**
     * @var \App\Lib\Passport
     */
    public $passport;

    /**
     * Acl constructor.
     *
     * @param $configPath string
     */
    public function __construct($configPath)
    {
        $this->config = Yaml::parse(file_get_contents($configPath));
    }

    /**
     * get Passport object
     *
     * @param Passport $passport
     * @return Passport
     */
    public function setPassport(\App\Lib\Passport $passport)
    {
        return $this->passport = $passport;
    }

    /**
     * Secure urls
     *
     * We simple regular expressions for preg_match
     *
     * @return bool
     */
    public function secure()
    {
        $request        = Request::createFromGlobals();
        $uri            = $request->getPathInfo();

        $result = true;

        foreach ($this->config as $key => $value) {

            foreach ($value as $arg => $exp) {

                $roles = explode('|', $exp[1]);

                foreach ($roles as $role) {

                    $expression = str_replace(
                        array('/', ':number'),
                        array("\\/", '[0-9]'),
                        $exp[0]
                    );
                    if (preg_match("/$expression/i", $uri)) {

                        //If authentication requested and user not authenticated
                        if (trim($role) == 'IS_AUTHENTICATED') {
                            if (false === $this->passport->isAuthenticated()) {
                                $result = false;
                            }

                        } else {
                            if (false === $this->passport->hasRole(trim($role))) {
                                $result = false;
                            }
                        }
                    }
                }
            }
        }
        return $result;
    }
}