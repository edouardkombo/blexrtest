<?php
/**
 * Created by Edouard Kombo.
 * @Author: Edouard Kombo
 * Date: 20/04/2016
 * Time: 10:42
 */

namespace App\Lib;

/**
 * @author: Edouard Kombo
 * Class Helper
 * Bring helper methods
 * @package App\Lib
 */
class Helper
{
    /**
     * constructor.
     */
    public function __construct()
    {
    }

    /**
     * Create datetime
     *
     * @param $format string
     * @param $time string
     * @return \Datetime
     */
    public function createDateFromFormat($format, $time)
    {
        $is_pm  = (stripos($time, 'PM') !== false);
        $time   = str_replace(array('AM', 'PM'), '', $time);
        $format = str_replace('A', '', $format);

        $date   = \DateTime::createFromFormat(trim($format), trim($time));

        if ($is_pm)
        {
            $date->modify('+12 hours');
        }

        return $date;

    }

    /**
     * Implements the booking logic
     *
     * @param $fromDate \DateTime
     * @param $toDate \DateTime
     * @param $sick boolean
     * @return array|boolean
     */
    public function bookingLogic($fromDate, $toDate, $sick)
    {
        $datas = true;

        //Convert these dates to timestamps
        $fromDateTimeStamp = $fromDate->getTimestamp();
        $toDateTimeStamp = $toDate->getTimestamp();

        //We calculate the timestamp of the day before start date
        $dayBeforeFromDate              = $fromDateTimeStamp - (24 * 60 * 60);
        $dayBeforeFromDateLess8Hours    = $dayBeforeFromDate - (8 * 60 * 60);

        $currentTimestamp = time();

        //If start date is greater than end date, or if end date is less than current date we show an error
        if ($fromDateTimeStamp < $currentTimestamp) {
            $datas = array('message' => 'Start date can not be less than current date', 'status' => 'error');

        } else if ($toDateTimeStamp < $currentTimestamp) {
            $datas = array('message' => 'End date can not be less than current date', 'status' => 'error');

        } else if (($fromDateTimeStamp > $toDateTimeStamp)) {
            $datas = array('message' => 'Start date can not be greater than ending date', 'status' => 'error');

        } else {

            //If employee not sick
            if (false === $sick) {

                //he has to book 8 hours before end of previous day
                if ($currentTimestamp > $dayBeforeFromDateLess8Hours) {
                    $datas = array(
                        'message' => 'You must book your work at home request, 8 hours before the end of the previous requested day',
                        'status' => 'error'
                    );
                }
            } else {
                //Employee is sick, he can book this day at 8 AM minimum
                $WorkingDateAt8AMString = $fromDate->format('Y-m-d') . " 08:00";
                $WorkingDateAt8AM = \DateTime::createFromFormat('Y-m-d H:i', $WorkingDateAt8AMString);
                $WorkingDateAt8AMTimestamp = $WorkingDateAt8AM->getTimestamp();

                //If he attempts to book before AM,
                if ($WorkingDateAt8AMTimestamp < $currentTimestamp) {

                    $datas = array(
                        'message' => 'You have to book at minimum 8 AM this day, or you tried to book an invalid date. Please try again later',
                        'status' => 'error'
                    );
                }
            }
        }

        return $datas;
    }
}