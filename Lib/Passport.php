<?php
/**
 * Created by Edouard Kombo.
 * @Author: Edouard Kombo
 * Date: 20/04/2016
 * Time: 10:42
 */

namespace App\Lib;

/**
 * @author: Edouard Kombo
 * Class Passport
 * Authentication library for this app
 * @package App\Lib
 */
class Passport
{
    /**
     * @var array
     */
    public $datas = [];

    /**
     * Passport constructor.
     */
    public function __construct()
    {
        if (isset($_SESSION['user']) && (unserialize($_SESSION['user']) instanceof \App\Entity\User)) {
            $this->datas['user'] = unserialize($_SESSION['user']);
        } else {
            $this->datas = $_SESSION;
        }
    }

    /**
     * Authenticate user
     *
     * @param \App\Entity\User $user
     * @return bool
     */
    public function authenticate(\App\Entity\User $user)
    {
        if ($user instanceof \App\Entity\User) {
            $_SESSION['user'] = serialize($user);
            $this->datas['user'] = unserialize($_SESSION['user']);
            $result = true;
        } else {
            $result = false;
        }

        return (boolean) $result;

    }

    /**
     * Retrieve current user
     *
     * @return object
     */
    public function getUser()
    {
        return (object) $this->datas['user'];
    }

    /**
     * Check if user is logged in
     *
     * @return bool
     */
    public function isAuthenticated()
    {
        return (boolean) (isset($this->datas['user']) && ($this->datas['user'] instanceof \App\Entity\User)) ? true : false;
    }


    public function hasRole($role)
    {
        if (true === $this->isAuthenticated()) {
            return (boolean) ($this->getUser()->getRole() === $role) ? true : false;
        }
    }

    /**
     * Check if user is logged in
     *
     * @return bool
     */
    public function logOut()
    {
        $this->datas = '';
        session_unset();
        session_destroy();

        return (boolean) (empty($_SESSION) && empty($this->datas)) ? true : false;
    }

}