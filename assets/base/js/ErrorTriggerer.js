//Method that will show specific errors inside div
function triggerError(div, message) {

    //reset divs value
    var errorDiv = document.getElementById('error');
    var successDiv = document.getElementById('success');

    errorDiv.innerHTML = '';
    errorDiv.style.display  = 'none';
    successDiv.innerHTML = '';
    successDiv.style.display = 'none';

    if (div === 'error') {
        errorDiv.style.display = "block";
        errorDiv.innerHTML = message;
    } else {
        successDiv.style.display = "block";
        successDiv.innerHTML = message;
    }
}