/**
 * @author: Edouard Kombo
 *
 * Custom helpers to manage javascript validation side
 */

/**
 * Get url parameters
 *
 * @returns {Array}
 */
function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

/**
 *Check if string is on date format
 *
 * @param string date
 * @returns {boolean}
 */
function isDate(date) {
    return (new Date(date) !== "Invalid Date" && !isNaN(new Date(date)) ) ? true : false;
}

//Method that will show specific errors inside div
function triggerError(div, message) {

    //reset divs value
    var errorDiv = document.getElementById('error');
    var successDiv = document.getElementById('success');

    errorDiv.innerHTML = '';
    errorDiv.style.display  = 'none';
    successDiv.innerHTML = '';
    successDiv.style.display = 'none';

    if (div === 'error') {
        errorDiv.style.display = "block";
        errorDiv.innerHTML = message;
    } else {
        successDiv.style.display = "block";
        successDiv.innerHTML = message;
    }
}

//Send request to controller, show specific message or redirect
function send(method, url, params) {

    var xhr = new XMLHttpRequest();
    xhr.open(method, url, true);
    xhr.onload = function () {
        // do something to response
        var response = JSON.parse(this.responseText);

        if (response.status === 'error') {
            triggerError(response.status, response.message);

        } else if (response.status === 'success' && (typeof response.redirect !== 'undefined')) {
            document.location.href= ""+response.redirect+""

        } else if (response.status === 'success' && (typeof response.redirect === 'undefined')) {
            triggerError(response.status, response.message);

        }
    };
    xhr.send(params);
}