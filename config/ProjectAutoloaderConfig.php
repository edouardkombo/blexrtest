<?php

/**
 * @Author: Edouard Kombo
 *
 * Namespaces have been added to the project, it makes it easy to call autoloaders instead of the old method
 * Default namespace is App and is defined at the toot of the project
 *
 * @param string $className
 */
function projectAutoloader($className) {

    $vars1 = (array) ['App', '\\'];
    $vars2 = (array) ['', '/'];
    $newClassName = (string) __ROOT_PATH__ . str_replace($vars1, $vars2, $className) . '.php';

    try {
        if(file_exists($newClassName)) {
            require_once($newClassName);
        } else {
            throw new Exception("Class $className not found !");
        }
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

spl_autoload_register('projectAutoloader');